@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <ul class="nav nav-tabs mb-3 justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true" pill="1" >Step 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false" pill="2">Step 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false" pill="3">Step 3</a>
                </li>
            </ul>

            <div class="card">
                <form id="form-signup" method="POST" action="{{ route('register-user') }}">
                    @csrf
                
                    <div class="tab-content card-body" id="pills-tabContent">
                        {{-- STEP 1 --}}
                        <div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab">
                            <div class="form-group has row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" name="first_name" id="first_name" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" name="last_name" id="last_name" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                <div class="col-md-7">
                                    <input type="email" class="form-control" name="email" id="email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-7">
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="re-password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                                <div class="col-md-7">
                                    <input type="password" class="form-control" id="re-password">
                                </div>
                            </div>

                            <div class="form-check row">
                                <div class="col-md-7 offset-md-4">
                                    <input type="checkbox" class="form-check-input" id="term">
                                    <label class="form-check-label" for="term">Term and Condition</label>
                                </div>
                            </div>
                
                        </div>

                        {{-- STEP 2 --}}
                        <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                            <div class="form-group row">
                                <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>
                                <div class="col-md-6">
                                    <textarea name="alamat[]" class="form-control" rows="3"></textarea>
                                </div>
                                <div class="col-md-1">
                                    <button id="btn_add_textarea" class="btn btn-success" type="button">+</button>
                                </div>
                                
                            </div>

                            <div id="add-more" class="form-group row"></div>

                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">Date of Birth</label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="dob" id="date">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="member_type" class="col-md-4 col-form-label text-md-right">Membership</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="member_type" id="member_type">
                                        <option>Silver</option>
                                        <option>Gold</option>
                                        <option>PLatinum</option>
                                        <option>Black</option>
                                        <option>VIP</option>
                                        <option>VVIP</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit_card" class="col-md-4 col-form-label text-md-right">Credit Card</label>
                                <div class="col-md-6" style="margin-bottom:1.1rem;">
                                    <input type="number" class="form-control" name="credit_card" id="credit_card">
                                </div>
                                <div class="col-md-6 offset-md-4" style="margin-bottom:1.1rem;">
                                    <div class="form-group row">
                                        <label for="month" class="col-md-4 col-form-label text-md-right">Month</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="month" id="month">
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                                <option>05</option>    
                                                <option>06</option>
                                                <option>07</option>
                                                <option>08</option>
                                                <option>09</option>
                                                <option>10</option>
                                                <option>11</option>    
                                                <option>12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="year" class="col-md-4 col-form-label text-md-right">Year</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="year" id="year">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cvc" class="col-md-4 col-form-label text-md-right">CVC</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="cvc" id="cvc">
                                        </div>
                                    </div>
                                </div>
                            </div>

            
                        </div>

                        {{-- STEP 3 --}}
                        <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis alias dolor hic molestiae, eligendi optio. Amet quos officia dolore, illo magnam quibusdam rem, adipisci quaerat in dignissimos, ratione suscipit optio.21212121</div>

                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-8">
                                <button type="button" id="back" class="btn btn-warning invisible">Back</button>
                                <button type="button" id="next" class="btn btn-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>

@endsection

@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script> --}}
{{-- <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script> --}}
<script>
$(function(){

    $("#next").click(function(){
        var tab = $("ul#pills-tab li a.active");
        var pill = tab.attr('pill');
        // console.log(pill);
        if (pill == 1) {
            var valid = checkPill_1();
            if (valid === 1) {
                $('.nav-tabs a[href="#pills-2"]').tab('show');
            }
        }
        else if (pill == 2){
            $("#form-signup").submit();
        }
    });

    $("#btn_add_textarea").click(function(){
        $("#add-more").append(
            "<div class='col-md-6 offset-md-4' style='margin-bottom:1.1rem;'><textarea name=alamat[] class=form-control rows=3></textarea></div>"
        );
    });


    function checkPill_1(){
        var valid=1;
        var ln = $("#last_name");
        var pass = $("#password");
        var repass = $("#re-password");
        var email = $("#email");
        var term = $("#term");

        if (ln.val() == '') {
            ln.addClass('is-invalid');
            valid=0;
        }
        else ln.removeClass('is-invalid');

        if (pass.val() == '') {
            pass.addClass('is-invalid');
            valid=0;
        }
        else pass.removeClass('is-invalid');

        if (repass.val() == '') {
            repass.addClass('is-invalid');
            valid=0;
        }
        else repass.removeClass('is-invalid');

        if (pass.val() != '' && repass.val() != ''){
            if (pass.val() === repass.val()){
                pass.removeClass('is-invalid');
                repass.removeClass('is-invalid');
            }
            else {
                pass.addClass('is-invalid');
                repass.addClass('is-invalid');
                valid=0;
            }
        }
        
        if (validateEmail(email.val())) email.removeClass('is-invalid');
        else {
            email.addClass('is-invalid');
            valid=0;
        }
        
        if (!term[0].checked){
            term.addClass('is-invalid');
            valid=0;
        }
        else term.removeClass('is-invalid');

        return valid;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
});
</script>
@endpush