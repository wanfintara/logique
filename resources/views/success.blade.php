@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('login') }}" class="btn btn-success btn-block">Login</a>
                </div>
            
            </div>

            
        </div>
    </div>
</div>

@endsection