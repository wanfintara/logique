<?php

function cariMax($bilangan){
    $max = 0;
  
    foreach ($bilangan as $b){
        if ($max < $b) $max = $b;
    }
    return $max;
}

$bilangan = array(11, 6, 31, 201, 99, 861, 1, 7, 14, 79);
$hasil = cariMax($bilangan);

echo $hasil;
?>