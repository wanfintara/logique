<?php
function cekPrima($bilangan){
    if ($bilangan == 1) return false;
     
    for ($i = 2; $i <= sqrt($bilangan); $i++){
        if ($bilangan % $i == 0) return false;
    }
    return true;
}
 
$bilangan = 97;
$hasil = cekPrima($bilangan);

if ($hasil)
    echo $bilangan, " : Bilangan Prima";
else
    echo $bilangan, " : Bukan Bilangan Prima";
?>