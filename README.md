<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Logique Technical Test - Installation Steps

- Extract logique.rar to htdocs (local server)
- Open logique folder and copy .env.example and rename to .env
- Setup database in .env file, DB_DATABASE - DB_USERNAME - DB_PASSWORD
- Copy database data (logique.sql) to database
- Open CMD / Terminal, go to logique folder and run "composer update"
- CMD / Terminal : run "php artisan key:generate"
- Then open browser localhost/logique/public

