<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class RegisterUserController extends Controller
{
    protected function register(Request $data)
    {
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->date_of_birth = $data['dob'];
        // $user->membership = $data['member_type'];
        $user->save();

        $now = Carbon::now();

        foreach($data['alamat'] as $value){
            $alamat[] = [
                'address' => $value,
                'user_id' => $user->id,
                'created_at' => $now
            ];
        }
        DB::table('addresses')->insert($alamat);

        return view('success');
    }
}
